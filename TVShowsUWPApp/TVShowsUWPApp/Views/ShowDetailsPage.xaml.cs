﻿using TVShowsUWPApp.ViewModels;

namespace TVShowsUWPApp.Views
{
    public sealed partial class ShowDetailsPage
    {
        public ShowDetailsPage()
        {
            InitializeComponent();
        }

        public ShowDetailsPageViewModel ViewModel => (ShowDetailsPageViewModel)DataContext;

    }
}
