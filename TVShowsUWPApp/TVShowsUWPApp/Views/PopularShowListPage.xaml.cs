﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TVShowsUWPApp.ViewModels;

namespace TVShowsUWPApp.Views
{
    public sealed partial class PopularShowListPage
    {
        public PopularShowListPage()
        {
            this.InitializeComponent();
        }

        public PopularShowListPageViewModel ViewModel => (PopularShowListPageViewModel)DataContext;
    }
}
