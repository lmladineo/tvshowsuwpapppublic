﻿using TVShowsUWPApp.Models.Enums;
using TVShowsUWPApp.Models.NavigationParameters;
using TVShowsUWPApp.Services.Interface;

namespace TVShowsUWPApp.Services
{
    public class NavigationService : INavigationService
    {
        private readonly Prism.Windows.Navigation.INavigationService _prismNavigationService;

        public NavigationService(Prism.Windows.Navigation.INavigationService prismNavigationService)
        {
            _prismNavigationService = prismNavigationService;
        }

        public bool Navigate(PageTokens pageToken, BaseNavigationParameters navigationParameters) =>
            _prismNavigationService.Navigate(pageToken.ToString(), navigationParameters.ToNavigationParameters());

        #region Prism.Windows.Navigation.INavigationService

        public bool Navigate(string pageToken, object parameter) =>
            _prismNavigationService.Navigate(pageToken, parameter);

        public void GoBack() => _prismNavigationService.GoBack();

        public bool CanGoBack() => _prismNavigationService.CanGoBack();

        public void GoForward() => _prismNavigationService.GoForward();

        public bool CanGoForward() => _prismNavigationService.CanGoForward();

        public void ClearHistory() => _prismNavigationService.ClearHistory();

        public void RemoveFirstPage(string pageToken = null, object parameter = null) =>
            _prismNavigationService.RemoveFirstPage(pageToken, parameter);

        public void RemoveLastPage(string pageToken = null, object parameter = null) =>
            _prismNavigationService.RemoveLastPage(pageToken, parameter);

        public void RemoveAllPages(string pageToken = null, object parameter = null) =>
            _prismNavigationService.RemoveAllPages(pageToken, parameter);

        public void RestoreSavedNavigation() => _prismNavigationService.RestoreSavedNavigation();

        public void Suspending() => _prismNavigationService.Suspending();

        #endregion
    }
}
