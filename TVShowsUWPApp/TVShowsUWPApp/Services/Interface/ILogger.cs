﻿using System;
using System.Runtime.CompilerServices;

namespace TVShowsUWPApp.Services.Interface
{
    public interface ILogger
    {
        // TRACING
        void TraceDebug(string message);
        void TraceDebugFormat(string message, params object[] args);
        void TraceInfo(string message);
        void TraceInfoFormat(string message, params object[] args);
        void TraceWarning(string message);
        void TraceWarningFormat(string message, params object[] args);
        void TraceError(string message, Exception ex = null);
        void TraceErrorFormat(string message, params object[] args);
        void TraceFatal(string message, Exception ex = null);
        void TraceFatalFormat(string message, params object[] args);
        void TraceMethod(string className, [CallerMemberName] string methodName = "");
        void TraceMethodWithMessage(string message, string className, [CallerMemberName] string methodName = "");
        void TraceMethodStart(string className, [CallerMemberName] string methodName = "");
        void TraceMethodStartWithMessage(string message, string className, [CallerMemberName] string methodName = "");
        void TraceMethodEnd(string className, [CallerMemberName] string methodName = "");
        void TraceMethodEndWithMessage(string message, string className, [CallerMemberName] string methodName = "");
    }
}
