﻿namespace TVShowsUWPApp.Services.Interface
{
    public interface IViewModelAggregateService
    {
        ILogger Logger { get; }

        INavigationService NavigationService { get; }

        IResiliency Resiliency { get; }
    }
}
