﻿using System.Threading.Tasks;
using TraktApiSharp.Requests.Params;
using TVShowsUWPApp.Models;
using TVShowsUWPApp.Models.Business;

namespace TVShowsUWPApp.Services.Interface
{
    public interface ITraktShowsService
    {
        Task<Show> GetShowAsync(string showId, TraktExtendedInfo extendedInfo = null,
            bool withAdditionalContent = false);

        Task<PaginationList<Show>> GetPopularShowsAsync(TraktExtendedInfo extendedInfo = null,
            TraktShowFilter showFilter = null, int? whichPage = null, int? limitPerPage = null);
    }
}