﻿using TVShowsUWPApp.Models.Enums;
using TVShowsUWPApp.Models.NavigationParameters;

namespace TVShowsUWPApp.Services.Interface
{
    public interface INavigationService : Prism.Windows.Navigation.INavigationService
    {
        bool Navigate(PageTokens pageToken, BaseNavigationParameters navigationParameters);
    }
}
