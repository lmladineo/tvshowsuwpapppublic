﻿using System;
using System.Threading.Tasks;
using TVShowsUWPApp.Models.Base;

namespace TVShowsUWPApp.Services.Interface
{
    public interface IResiliency
    {
        void Try(Action action);

        void Try(Action action, Action finallyAction);

        Task TryAsync(Func<Task> actionAsync);

        Task TryAsync(Wrapper<bool> isBusy, Func<Task> actionAsync);

        Task TryAsync(Func<Task> actionAsync, Action finallyAction);

        Task TryAsync(Wrapper<bool> isBusy, Func<Task> actionAsync, Action finallyAction);
    }
}
