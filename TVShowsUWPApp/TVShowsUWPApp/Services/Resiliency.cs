using System;
using System.Threading.Tasks;
using TVShowsUWPApp.Models.Base;
using TVShowsUWPApp.Services.Interface;

namespace TVShowsUWPApp.Services
{
    public class Resiliency : IResiliency
    {
        public void Try(Action action) => Try(action, () => { });

        public void Try(Action action, Action finallyAction)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            try
            {
                action();
            }
            catch (Exception ex)
            {
                Task.FromResult(ExceptionAction(ex));
            }
            finally
            {
                finallyAction();
            }
        }

        public async Task TryAsync(Func<Task> actionAsync) => await TryAsync(actionAsync, () => { });

        public async Task TryAsync(Wrapper<bool> isBusy, Func<Task> actionAsync) => await TryAsync(isBusy, actionAsync, () => { });

        public async Task TryAsync(Func<Task> actionAsync, Action finallyAction)
        {
            if (actionAsync == null)
                throw new ArgumentNullException(nameof(actionAsync));

            try
            {
                await actionAsync();
            }
            catch (Exception ex)
            {
                await ExceptionAction(ex);
            }
            finally
            {
                finallyAction();
            }
        }

        public async Task TryAsync(Wrapper<bool> isBusy, Func<Task> actionAsync, Action finallyAction)
        {
            if (actionAsync == null)
                throw new ArgumentNullException(nameof(actionAsync));

            if (isBusy.Value) return;

            try
            {
                isBusy.Value = true;

                await actionAsync();
            }
            catch (Exception ex)
            {
                await ExceptionAction(ex);
            }
            finally
            {
                finallyAction();

                isBusy.Value = false;
            }
        }

        private async Task ExceptionAction(Exception exception)
        {
            // TODO: Show dialog...
            await Task.FromResult(0);
        }
    }
}