﻿using System;
using TVShowsUWPApp.Services.Interface;

namespace TVShowsUWPApp.Services
{
    public class Logger : ILogger
    {
        //private static readonly string MethodStartTemplate = "{0}.{1}() - START";
        //private static readonly string MethodTemplate = "{0}.{1}()";
        //private static readonly string MethodEndTemplate = "{0}.{1}() - END";

        public void TraceDebug(string message)
        {
            // TODO: Not implemented
        }

        public void TraceDebugFormat(string message, params object[] args)
        {
            // TODO: Not implemented
        }

        public void TraceInfo(string message)
        {
            // TODO: Not implemented
        }

        public void TraceInfoFormat(string message, params object[] args)
        {
            // TODO: Not implemented
        }

        public void TraceWarning(string message)
        {
            // TODO: Not implemented
        }

        public void TraceWarningFormat(string message, params object[] args)
        {
            // TODO: Not implemented
        }

        public void TraceError(string message, Exception ex = null)
        {
            // TODO: Not implemented
        }

        public void TraceErrorFormat(string message, params object[] args)
        {
            // TODO: Not implemented
        }

        public void TraceFatal(string message, Exception ex = null)
        {
            // TODO: Not implemented
        }

        public void TraceFatalFormat(string message, params object[] args)
        {
            // TODO: Not implemented
        }

        public void TraceMethod(string className, string methodName = "")
        {
            // TODO: Not implemented
        }

        public void TraceMethodWithMessage(string message, string className, string methodName = "")
        {
            // TODO: Not implemented
        }

        public void TraceMethodStart(string className, string methodName = "")
        {
            // TODO: Not implemented
        }

        public void TraceMethodStartWithMessage(string message, string className, string methodName = "")
        {
            // TODO: Not implemented
        }

        public void TraceMethodEnd(string className, string methodName = "")
        {
            // TODO: Not implemented
        }

        public void TraceMethodEndWithMessage(string message, string className, string methodName = "")
        {
            // TODO: Not implemented
        }
    }
}
