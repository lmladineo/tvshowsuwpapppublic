﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TraktApiSharp;
using TraktApiSharp.Objects.Get.Shows;
using TraktApiSharp.Requests.Params;
using TVShowsUWPApp.Models;
using TVShowsUWPApp.Models.Business;
using TVShowsUWPApp.Models.ModelConverter;
using TVShowsUWPApp.Services.Interface;

namespace TVShowsUWPApp.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class TraktShowsService : ITraktShowsService
    {
        private readonly TraktClient _traktClient;

        public TraktShowsService(TraktClient traktClient)
        {
            _traktClient = traktClient;
        }

        public async Task<Show> GetShowAsync(string showId, TraktExtendedInfo extendedInfo = null,
            bool withAdditionalContent = false)
        {
            var traktShow = await _traktClient.Shows.GetShowAsync(showId, extendedInfo);
            if (traktShow == null) return null;

            var show = ShowModelConverter.Convert<Show>(traktShow);
            if (show == null) return null;

            if (!withAdditionalContent) return show;

            var aliases = await _traktClient.Shows.GetShowAliasesAsync(showId);
            show.Aliases = new ObservableCollection<TraktShowAlias>(aliases);

            var translations = await _traktClient.Shows.GetShowTranslationsAsync(showId);
            show.Translations = new ObservableCollection<TraktShowTranslation>(translations);

            show.ShowRating = await _traktClient.Shows.GetShowRatingsAsync(showId);
            show.Statistics = await _traktClient.Shows.GetShowStatisticsAsync(showId);

            return show;
        }

        public async Task<PaginationList<Show>> GetPopularShowsAsync(TraktExtendedInfo extendedInfo = null,
            TraktShowFilter showFilter = null, int? whichPage = null, int? limitPerPage = null)
        {
            var traktResults =
                await _traktClient.Shows.GetPopularShowsAsync(extendedInfo, showFilter, whichPage, limitPerPage);

            var results = new PaginationList<Show>
            {
                CurrentPage = traktResults.Page,
                TotalPages = traktResults.PageCount,
                LimitPerPage = traktResults.Limit,
                TotalItemCount = traktResults.ItemCount,
                TotalUserCount = traktResults.UserCount,
                Items = new ObservableCollection<Show>()
            };


            foreach (var traktPopularShow in traktResults)
            {
                var popularShow = ShowModelConverter.Convert<Show>(traktPopularShow);

                if (popularShow != null) results.Items.Add(popularShow);
            }

            return results;
        }
    }
}
