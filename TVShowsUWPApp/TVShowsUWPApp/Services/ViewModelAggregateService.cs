﻿using TVShowsUWPApp.Services.Interface;

namespace TVShowsUWPApp.Services
{
    public class ViewModelAggregateService : IViewModelAggregateService
    {
        public ILogger Logger { get; }
        public INavigationService NavigationService { get; }
        public IResiliency Resiliency { get; }

        public ViewModelAggregateService(ILogger logger, INavigationService navigationService, IResiliency resiliency)
        {
            Logger = logger;
            NavigationService = navigationService;
            Resiliency = resiliency;
        }
    }
}