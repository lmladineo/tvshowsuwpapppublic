﻿namespace TVShowsUWPApp.Models.Base
{
    public class Wrapper<T> 
    {
        public T Value { get; set; }
    }
}
