﻿using System.Collections.ObjectModel;
using TraktApiSharp.Objects.Basic;
using TraktApiSharp.Objects.Get.Shows;

namespace TVShowsUWPApp.Models.Business
{
    public class Show : TraktShow
    {
        public ObservableCollection<TraktShowAlias> Aliases { get; set; }

        public ObservableCollection<TraktShowTranslation> Translations { get; set; }

        public TraktRating ShowRating { get; set; }

        public TraktStatistics Statistics { get; set; }
    }
}
