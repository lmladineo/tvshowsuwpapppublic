﻿namespace TVShowsUWPApp.Models.Enums
{
    public enum PageTokens
    {
        PopularShowList,
        ShowDetails,
    }
}
