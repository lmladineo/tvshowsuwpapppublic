﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using TraktApiSharp.Enums;
using TraktApiSharp.Objects.Basic;
using TraktApiSharp.Objects.Get.Shows;

namespace TVShowsUWPApp.Models.ModelConverter
{
    public static class ShowModelConverter
    {
        private static readonly Random Randomiser = new Random();

        private const string DefaultImage = "ms-appx:///Assets/StoreLogo.jpg";

        private const string DefaultShowImageTemplateFormat = "ms-appx:///Assets/Images/image_{0}.jpg";
        private const string DefaultShowThumbTemplateFormat = "ms-appx:///Assets/Thumbs/thumb_{0}.png";

        public static T Convert<T>(TraktShow traktShow) where T : TraktShow, new()
        {
            if (traktShow == null)
                return null;

            var show = Activator.CreateInstance<T>();

            if (show == null)
                return null;

            show.AvailableTranslationLanguageCodes = traktShow.AvailableTranslationLanguageCodes ?? new List<string>();
            show.Certification = traktShow.Certification ?? string.Empty;
            show.Genres = traktShow.Genres ?? new List<string>();
            show.Homepage = traktShow.Homepage ?? string.Empty;
            show.LanguageCode = traktShow.LanguageCode ?? string.Empty;
            show.Overview = traktShow.Overview ?? string.Empty;
            show.Rating = traktShow.Rating ?? 0.0f;
            show.Runtime = traktShow.Runtime ?? 0;
            show.Title = traktShow.Title ?? string.Empty;
            show.Trailer = traktShow.Trailer ?? string.Empty;
            show.Votes = traktShow.Votes ?? 0;
            show.Year = traktShow.Year ?? 1900;
            show.Ids = traktShow.Ids;
            show.UpdatedAt = traktShow.UpdatedAt;
            show.AiredEpisodes = traktShow.AiredEpisodes ?? 0;
            show.CountryCode = traktShow.CountryCode ?? string.Empty;
            show.FirstAired = traktShow.FirstAired;
            show.Network = traktShow.Network ?? string.Empty;
            show.Status = traktShow.Status ?? TraktShowStatus.Unspecified;

            show.Images = GetDummyImages();

            return show as T;
        }

        private static TraktShowImages GetDummyImages()
        {
            var imagesTuple = GetRandomImages();

            return new TraktShowImages
            {
                Thumb = new TraktImage { Full = imagesTuple.Item1 },
                Logo = new TraktImage { Full = imagesTuple.Item2 },
                ClearArt = new TraktImage { Full = imagesTuple.Item3 },
                Banner = new TraktImage { Full = imagesTuple.Item4 }
            };
        }

        private static Tuple<string, string, string, string> GetRandomImages()
        {
            Func<int> getRandom = () => Randomiser.Next(1, 9);

            return new Tuple<string, string, string, string>(
                string.Format(DefaultShowThumbTemplateFormat, getRandom()),
                string.Format(DefaultShowImageTemplateFormat, getRandom()),
                string.Format(DefaultShowImageTemplateFormat, getRandom()),
                string.Format(DefaultShowImageTemplateFormat, getRandom()));
        }
    }
}
