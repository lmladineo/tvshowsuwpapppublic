﻿using System.Collections.Generic;

namespace TVShowsUWPApp.Models.NavigationParameters
{
    public class PopularShowListNavigationParameters : BaseNavigationParameters
    {
        public PopularShowListNavigationParameters()
        {
            
        }

        public PopularShowListNavigationParameters(object parameter)
        {
            
        }

        public override IDictionary<string, object> ToNavigationParameters()
        {
            return null;
        }
    }
}
