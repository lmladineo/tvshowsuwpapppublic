﻿using System.Collections.Generic;

namespace TVShowsUWPApp.Models.NavigationParameters
{
    public abstract class BaseNavigationParameters
    {
        public abstract IDictionary<string, object> ToNavigationParameters();
    }
}

// --- Example of NavigationParameteters model ---

//public class AgreementReadNavigationParameters : BaseNavigationParametersModel
//{
//    public long AgreementId { get; set; }

//    public AgreementReadNavigationParameters(long agreementId)
//    {
//        AgreementId = agreementId;
//    }

//    public AgreementReadNavigationParameters(Prism.Navigation.NavigationParameters navigationParameters)
//    {
//        long agreementId;
//        navigationParameters.TryGetValue(nameof(AgreementId), out agreementId);

//        AgreementId = agreementId;
//    }

//    public override Prism.Navigation.NavigationParameters ToNavigationParameters()
//    {
//        return new Prism.Navigation.NavigationParameters()
//        {
//            {nameof(AgreementId), AgreementId}
//        };
//    }
//}