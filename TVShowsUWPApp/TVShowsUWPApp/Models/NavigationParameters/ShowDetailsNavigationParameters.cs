﻿using System.Collections.Generic;

namespace TVShowsUWPApp.Models.NavigationParameters
{
    public class ShowDetailsNavigationParameters : BaseNavigationParameters
    {
        public string ShowId { get; }

        public ShowDetailsNavigationParameters(string showId)
        {
            ShowId = showId;
        }

        public ShowDetailsNavigationParameters(object parameter)
        {
            var dict = parameter as Dictionary<string, object>;
            if (dict == null) return;

            object showIdAsObject;
            dict.TryGetValue(nameof(ShowId), out showIdAsObject);

            ShowId = showIdAsObject as string;
        }

        public override IDictionary<string, object> ToNavigationParameters()
        {
            return new Dictionary<string, object>()
            {
                {nameof(ShowId), ShowId}
            };
        }
    }
}
