﻿using System;
using Windows.UI.Xaml.Data;

namespace TVShowsUWPApp.Utility.Converters
{
    public class StringFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var format = parameter?.ToString() ?? string.Empty;

            return string.Format(format, new[] { value });
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
