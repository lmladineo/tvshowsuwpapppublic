﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TVShowsUWPApp.Utility.Extensions
{
    public static class JsonExtensions
    {

        private static readonly JsonSerializerSettings JsonSerializerSettingsDefault = new JsonSerializerSettings
        {
            Converters = new List<JsonConverter> { new StringEnumConverter() }
        };

        private static readonly JsonSerializerSettings JsonSerializerSettingCircularReferencesDefault = new JsonSerializerSettings
        {
            Converters = new List<JsonConverter> { new StringEnumConverter() },
            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        };

        /// <summary>
        /// Converts given object to JSON formated string.
        /// </summary>
        /// <param name="o">Object to serialize.</param>
        /// <param name="indented">Formating. If <c>true</c> string is Indented;otherwise not.</param>
        /// <param name="nullvalue">Value to return if null.</param>
        /// <returns>Returns JSON formated string from given object.</returns>
        public static string ToJson(this object o, bool indented = false, string nullvalue = "")
        {
            if (o == null) return nullvalue;

            var formatting = indented ? Formatting.Indented : Formatting.None;
            return JsonConvert.SerializeObject(o, formatting, JsonSerializerSettingsDefault);
        }

        public static string ToJsonWithRef(this object o, bool indented = false, string nullvalue = "")
        {
            if (o == null) return nullvalue;

            var formatting = indented ? Formatting.Indented : Formatting.None;
            return JsonConvert.SerializeObject(o, formatting, JsonSerializerSettingCircularReferencesDefault);
        }

    }
}
