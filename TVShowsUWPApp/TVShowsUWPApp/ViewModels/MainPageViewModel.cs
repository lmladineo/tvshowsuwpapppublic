﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Prism.Windows.Navigation;
using TVShowsUWPApp.Models.Enums;
using TVShowsUWPApp.Models.NavigationParameters;
using TVShowsUWPApp.Services.Interface;
using TVShowsUWPApp.ViewModels.Base;

namespace TVShowsUWPApp.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        public MainPageViewModel(IViewModelAggregateService viewModelAggregateService) : base(viewModelAggregateService)
        {
        }

        public override async void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            await Task.FromResult(true);
        }

        public void OpenShowList()
        {
            NavigationService.Navigate(PageTokens.PopularShowList, new PopularShowListNavigationParameters());
        }
    }
}
