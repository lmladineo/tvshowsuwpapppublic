﻿using System.Threading.Tasks;

namespace TVShowsUWPApp.ViewModels.Interfaces
{
    public interface IRefreshable
    {
        Task RefreshAsync();
    }
}
