﻿using System.Threading.Tasks;
using TraktApiSharp.Requests.Params;
using TVShowsUWPApp.Models.Business;
using TVShowsUWPApp.Models.NavigationParameters;
using TVShowsUWPApp.Services.Interface;
using TVShowsUWPApp.Utility.Extensions;
using TVShowsUWPApp.ViewModels.Base;

namespace TVShowsUWPApp.ViewModels
{
    public class ShowDetailsPageViewModel : DetailsViewModel<string, Show>
    {
        private readonly ITraktShowsService _traktShowsService;

        public ShowDetailsPageViewModel(IViewModelAggregateService viewModelAggregateService,
            ITraktShowsService traktShowsService) : base(viewModelAggregateService)
        {
            _traktShowsService = traktShowsService;
        }

        protected override string OnNavigatedToOperationAction(object parameters)
        {
            var navigationParameters = new ShowDetailsNavigationParameters(parameters);

            return navigationParameters.ShowId;
        }

        protected override async Task GetItemsOperationActionAsync(string id)
        {
            Item = await _traktShowsService.GetShowAsync(id, new TraktExtendedInfo().SetFull(), true);

            Logger.TraceMethodWithMessage($"Data: {Item?.ToJson()}", ClassName);
        }
    }
}
