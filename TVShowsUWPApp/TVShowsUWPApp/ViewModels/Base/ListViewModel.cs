﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Prism.Windows.Navigation;
using TVShowsUWPApp.Services.Interface;
using TVShowsUWPApp.ViewModels.Interfaces;

namespace TVShowsUWPApp.ViewModels.Base
{
    public abstract class ListViewModel<T> : BaseViewModel, IRefreshable
    {
        private ObservableCollection<T> _items;
        private T _selectedItem;
        private bool _isRefreshing;
        private bool _canLoadMore;

        public ListViewModel(IViewModelAggregateService viewModelAggregateService) : base(viewModelAggregateService)
        {
        }

        protected int PageNumber { get; set; }

        protected int? TotalPageNumber { get; set; } = 1;

        public ObservableCollection<T> Items
        {
            get { return _items; }
            set
            {
                if (_items == value) return;

                _items = value;
                RaisePropertyChanged();
            }
        }

        public T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != null && _selectedItem.Equals(value)) return;

                _selectedItem = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                if (_isRefreshing == value) return;

                _isRefreshing = value;
                RaisePropertyChanged();
            }
        }

        public bool CanLoadMore
        {
            get { return _canLoadMore; }
            set
            {
                if (_canLoadMore == value) return;

                _canLoadMore = value;
                RaisePropertyChanged();
            }
        }

        protected async Task GetItemsAsync(int pageNumber = 0)
        {
            await Resiliency.TryAsync(IsBusy,
                async () =>
                {
                    Logger.TraceMethodStart(ClassName);

                    PageNumber = pageNumber;

                    await GetItemsOperationActionAsync();
                },
                () => Logger.TraceMethodEnd(ClassName));
        }

        protected abstract Task GetItemsOperationActionAsync();

        #region INavigationAware

        public override async void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            OnNavigatedToOperationAction(e.Parameter);

            CanLoadMore = true;

            await GetItemsAsync();
        }

        #endregion

        /// <summary>
        /// Get info from navigation parameters.
        /// </summary>
        /// <param name="parameters"></param>
        protected abstract void OnNavigatedToOperationAction(object parameters);

        #region IRefreshable

        public async Task RefreshAsync()
        {
            if (IsRefreshing) return;

            IsRefreshing = true;
            CanLoadMore = true;

            await GetItemsAsync();

            IsRefreshing = false;
        }

        #endregion

        private async Task LoadMoreAsync()
        {
            await GetItemsAsync(PageNumber + 1);

            // PageNumber is starting to count from 0. TotalPageNumber is starting to count from 1.
            // Like in list -> ordinal number (PageNumber) and count (TotalPageNumber)....
            CanLoadMore = PageNumber + 1 < (TotalPageNumber ?? 1);
        }
    }
}
