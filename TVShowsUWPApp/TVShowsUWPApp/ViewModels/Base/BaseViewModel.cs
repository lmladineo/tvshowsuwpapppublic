﻿using Prism.Windows.Mvvm;
using TVShowsUWPApp.Models.Base;
using TVShowsUWPApp.Services.Interface;
using INavigationService = TVShowsUWPApp.Services.Interface.INavigationService;

namespace TVShowsUWPApp.ViewModels.Base
{
    public class BaseViewModel : ViewModelBase
    {
        private Wrapper<bool> _isBusy = new Wrapper<bool>();

        public BaseViewModel(IViewModelAggregateService viewModelAggregateService)
        {
            ClassName = GetType().FullName;

            Logger = viewModelAggregateService.Logger;
            NavigationService = viewModelAggregateService.NavigationService;
            Resiliency = viewModelAggregateService.Resiliency;
        }

        protected readonly string ClassName;

        protected readonly ILogger Logger;

        protected readonly INavigationService NavigationService;

        protected readonly IResiliency Resiliency;

        protected Wrapper<bool> IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy == value) return;

                _isBusy = value; 
                RaisePropertyChanged();
            }
        }
    }
}
