﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Prism.Windows.Navigation;
using TVShowsUWPApp.Services.Interface;
using TVShowsUWPApp.ViewModels.Interfaces;

namespace TVShowsUWPApp.ViewModels.Base
{
    public abstract class DetailsViewModel<TId, TItem> : BaseViewModel, IRefreshable
    {
        private TId _itemId;
        private TItem _item;
        private bool _isRefreshing;

        public DetailsViewModel(IViewModelAggregateService viewModelAggregateService) : base(viewModelAggregateService)
        {
        }

        public TId ItemId
        {
            get { return _itemId; }
            set
            {
                if (_itemId == null && value == null) return;
                if (_itemId != null && _itemId.Equals(value)) return;

                _itemId = value;
                RaisePropertyChanged();
            }
        }

        public TItem Item
        {
            get { return _item; }
            set
            {
                if (_item == null && value == null) return;
                if (_item != null && _item.Equals(value)) return;

                _item = value;
                RaisePropertyChanged();
            }
        }

        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                if (_isRefreshing == value) return;

                _isRefreshing = value;
                RaisePropertyChanged();
            }
        }

        protected async Task GetItemAsync(TId id)
        {
            Logger.TraceMethodStartWithMessage($"Id = {id}.", ClassName);

            if (EqualityComparer<TId>.Default.Equals(id, default(TId)))
            {
                // TODO: Some error shown...

                return;
            }

            ItemId = id;

            await Resiliency.TryAsync(IsBusy,
                async () =>
                {
                    await GetItemsOperationActionAsync(id);
                },
                () => Logger.TraceMethodEnd(ClassName));
        }

        protected abstract Task GetItemsOperationActionAsync(TId id);

        #region INavigationAware

        public override async void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            base.OnNavigatedTo(e, viewModelState);

            var id = OnNavigatedToOperationAction(e.Parameter);

            await GetItemAsync(id);
        }

        #endregion

        /// <summary>
        /// Get info from navigation parameters.
        /// </summary>
        /// <param name="parameters"></param>
        protected abstract TId OnNavigatedToOperationAction(object parameters);

        #region IRefreshable

        public async Task RefreshAsync()
        {
            if (IsRefreshing) return;

            IsRefreshing = true;

            await GetItemAsync(ItemId);

            IsRefreshing = false;
        }

        #endregion
    }
}
