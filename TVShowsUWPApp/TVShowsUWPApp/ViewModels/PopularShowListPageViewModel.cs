﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TraktApiSharp.Requests.Params;
using TVShowsUWPApp.Models.Business;
using TVShowsUWPApp.Models.Enums;
using TVShowsUWPApp.Models.NavigationParameters;
using TVShowsUWPApp.Services.Interface;
using TVShowsUWPApp.Utility.Extensions;
using TVShowsUWPApp.ViewModels.Base;

namespace TVShowsUWPApp.ViewModels
{
    public class PopularShowListPageViewModel : ListViewModel<Show>
    {
        private readonly ITraktShowsService _traktShowsService;

        public PopularShowListPageViewModel(IViewModelAggregateService viewModelAggregateService,
            ITraktShowsService traktShowsService) : base(viewModelAggregateService)
        {
            _traktShowsService = traktShowsService;
        }

        protected override void OnNavigatedToOperationAction(object parameters)
        {
            // Do nothing.
            //var navigationParameters = new PopularShowListNavigationParameters(parameters);
        }

        protected override async Task GetItemsOperationActionAsync()
        {
            var response = await _traktShowsService.GetPopularShowsAsync(new TraktExtendedInfo().SetFull(), null, PageNumber, 20);

            TotalPageNumber = response.TotalPages;
            Items = PageNumber == 0
                ? new ObservableCollection<Show>()
                : (Items ?? new ObservableCollection<Show>());

            foreach (var item in response.Items)
            {
                Items.Add(item);
            }

            Logger.TraceMethodWithMessage($"Data: {Items?.ToJson()}", ClassName);
        }

        public void OpenShowDetails()
        {
            if (SelectedItem == null) return;

            NavigationService.Navigate(PageTokens.ShowDetails,
                new ShowDetailsNavigationParameters(SelectedItem.Ids.Slug));
        }
    }
}
