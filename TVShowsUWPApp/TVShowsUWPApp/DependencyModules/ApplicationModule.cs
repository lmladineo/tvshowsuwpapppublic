﻿using Autofac;
using TraktApiSharp;
using TVShowsUWPApp.Services;
using TVShowsUWPApp.Services.Interface;

namespace TVShowsUWPApp.DependencyModules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Logger>()
                .As<ILogger>()
                .SingleInstance();

            builder.RegisterType<NavigationService>()
                .As<INavigationService>()
                .SingleInstance();

            builder.RegisterType<ViewModelAggregateService>()
                .As<IViewModelAggregateService>()
                .InstancePerDependency();

            builder.RegisterType<Resiliency>()
                .As<IResiliency>()
                .InstancePerDependency();

            builder.RegisterType<TraktShowsService>()
                .As<ITraktShowsService>()
                .InstancePerDependency();

            builder.Register<TraktClient>(context =>
                new TraktClient("324f5ca5b890917a6cac7451ee1df6148ebe9007c07347b9e9177e9ceada6b22",
                    "0b2cdae32b45221a5fe82f780c3447c96978eb4e609cc49399774796fb3f6114"));
        }
    }
}
